## Yoann Gimenez

## Création de CRUD en ligne de commande

1. Placer le terminal dans mvc6
2. ```bash
   php bin/console.php make controller
   ```
3. Suivre les instructions données par la console

## Notes importantes 

- Le CRUD remplace le fichier routes.php existant à chaque création de CRUD. Pour créer plusieurs CRUD, dupliquez ce fichier pour ensuite transverser les routes.
- Présentement, il n'existe que la méthode index dans le Controller.
- Le CRUD automatique utilise le layout app.

## Explication du code

- Dans console.php

```
<?php require_once ('core/Console/CreateCRUD.php');

echo "Lancement effectué \n";

if(!empty($argv[1]) && $argv[1] === 'make' && $argv[2] === 'controller'){
    $crud = new CreateCRUD();
    echo "Nom du Controller : ";
    $controller_name = ucfirst(trim(fgets(STDIN)));
    $crud->createController($controller_name);
    $crud->addRoutes($controller_name);
    $crud->createCRUDFiles($controller_name);
}
```
Le fichier appelle l'objet CreateCRUD ainsi que ses méthodes afin de créer le Controller avec le nom écrit par l'utilisateur, de modifier les routes et de créer les fichiers utilisant les méthodes du Controller (index, add, single, edit, delete)

- Dans core/Console/CreateCRUD.php
  - Création du Controller

```
<?php

class CreateCRUD{

    public function createController($controller_name){
        mkdir('template/app/'.lcfirst($controller_name));
        $directory = 'src/Controller/';
        $data = '<?php

        namespace App\Controller;
        
        use Core\Kernel\AbstractController;
        
        /**
         *
         */
        class '.$controller_name.'Controller extends AbstractController
        {
            public function index()
            {
                $'.lcfirst($controller_name).'s = // Ajouter la méthode du model 
                $this->render("app.'.lcfirst($controller_name).'.index",array(
                    "'.lcfirst($controller_name).'s" => $'.lcfirst($controller_name).'s,
                ));
            }
        }';
        file_put_contents($directory.$controller_name.'Controller.php', $data);
    }
```

Dans notre objet on retrouve la première méthode -> createController, ayant pour argument le nom du controlleur que l'utilisateur aura rentré.
On crée un dossier dans template/app pour stocker les fichiers index,edit,etc, dans ce dossier. Ensuite on va crée notre controller, on définit un $directory donc ici src/Controller/ ainsi que $data qui représente le contenu du fichier.
Bien évidemment on concatène pour que le nom du controller corresponde avec ce que l'utilisateur a écrit.
Présentement ce dernier ne possède que la méthode index.
file_put_contents permet de créer un fichier en utilisant 2 paramètres : Le pathing et le contenu du fichier.

 - Ajout des routes

``` 
public function addRoutes($controller_name){
        $data = '<?php
    $routes = array(
        array("home","default","index"),
        
        array("listing-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","index"),
        array("add-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","add"),
        array("single-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","single",array("id")),
        array("edit-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","edit",array("id")),
        array("delete-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","delete",array("id")),
        
    );';
        file_put_contents('config/routes.php',$data);
    }
```

On reprend le même principe qu'au dessus avec file_put_contents. Cependant on ne va pas créer de fichier mais le remplacer. Ce qui pose soucis si l'on souhaite crée plusieurs CRUD car le précédent CRUD sera remplacé.

 - Les fichiers dans template

``` 
public function createCRUDFiles($controller_name){
        $directory = 'template/app/'.lcfirst($controller_name);

        // Add Index
        $data = '<section id="index-'.lcfirst($controller_name).'s">
    <div class="wrap">
        <div class="liste-'.lcfirst($controller_name).'s">
            <h2>Liste des '.lcfirst($controller_name).'s</h2>
            <table>
                <thead>
                <tr>
                    <th>Info 1</th>
                    <th>Info 2</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($'.lcfirst($controller_name).'s as $'.lcfirst($controller_name).'){?>
                    <tr>
                        <td><?php echo $'.lcfirst($controller_name).'->info1; ?></td>
                        <td><?php echo $'.lcfirst($controller_name).'->info2; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
';
        file_put_contents($directory.'/index.php', $data);
    }
```

Pour compléter notre CRUD il faut crée les fichiers dans template. A nouveau on utilise file_put_contents et on récupère le directory qu'on a précédemment créé dans notre création de controller.
Ici un placeholder de l'index avec un tableau et un foreach déjà intégré. La structure est pré-construite pour faire gagner du temps et laisser à l'utilisateur la possibilité de modifier ce dernier afin que "info1" corresponde à la colonne de sa table SQL.

 - Modification du header pour accéder à notre CRUD

C'est la dernière étape de notre création de CRUD qui est présentement non-crée car le problème présent avec les routes revient ici aussi. 

## Pistes d'amélioration

- Vérifier que le fichier existe avant de le créer, cela pourrait éviter des potentielles suppressions de données
- Compléter le CRUD, ajouter les méthodes add, single, edit et delete.
- Pouvoir créer plusieurs CRUD sans que cela supprime les routes du CRUD précédent.
- Modifier le header pour avoir un accès direct à ce nouveau CRUD -> Problématique potentiellement identique du fichier routes. 
- Revoir l'écriture de la class CreateCRUD pour peut être plutôt faire passer $controller_name en __construct afin de pouvoir l'utiliser avec $this dans les méthodes.