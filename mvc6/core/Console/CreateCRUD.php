<?php

class CreateCRUD{

    public function createController($controller_name){
        mkdir('template/app/'.lcfirst($controller_name));
        $directory = 'src/Controller/';
        $data = '<?php

        namespace App\Controller;
        
        use Core\Kernel\AbstractController;
        
        /**
         *
         */
        class '.$controller_name.'Controller extends AbstractController
        {
            public function index()
            {
                $'.lcfirst($controller_name).'s = // Ajouter la méthode du model 
                $this->render("app.'.lcfirst($controller_name).'.index",array(
                    "'.lcfirst($controller_name).'s" => $'.lcfirst($controller_name).'s,
                ));
            }
        }';
        file_put_contents($directory.$controller_name.'Controller.php', $data);
    }

    public function addRoutes($controller_name){
        $data = '<?php
    $routes = array(
        array("home","default","index"),
        
        array("listing-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","index"),
        array("add-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","add"),
        array("single-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","single",array("id")),
        array("edit-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","edit",array("id")),
        array("delete-'.lcfirst($controller_name).'","'.lcfirst($controller_name).'","delete",array("id")),
        
    );';
        file_put_contents('config/routes.php',$data);
    }

    public function createCRUDFiles($controller_name){
        $directory = 'template/app/'.lcfirst($controller_name);

        // Add Index
        $data = '<section id="index-'.lcfirst($controller_name).'s">
    <div class="wrap">
        <div class="liste-'.lcfirst($controller_name).'s">
            <h2>Liste des '.lcfirst($controller_name).'s</h2>
            <table>
                <thead>
                <tr>
                    <th>Info 1</th>
                    <th>Info 2</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($'.lcfirst($controller_name).'s as $'.lcfirst($controller_name).'){?>
                    <tr>
                        <td><?php echo $'.lcfirst($controller_name).'->info1; ?></td>
                        <td><?php echo $'.lcfirst($controller_name).'->info2; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
';
        file_put_contents($directory.'/index.php', $data);
    }

}