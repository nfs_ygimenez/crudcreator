<?php
//  php bin/console.php make controller
require_once ('core/Console/CreateCRUD.php');

echo "Lancement effectué \n";

if(!empty($argv[1]) && $argv[1] === 'make' && $argv[2] === 'controller'){
    $crud = new CreateCRUD();
    echo "Nom du Controller : ";
    $controller_name = ucfirst(trim(fgets(STDIN)));
    $crud->createController($controller_name);
    $crud->addRoutes($controller_name);
    $crud->createCRUDFiles($controller_name);
}


